var ac = 0, tword = 0010101, io=0;

var xa0 = 0737770, 
    xb0 = 0000000, 
    xc0 = 0020000,
    ya0 = 0000000, 
    yb0 = 0040000, 
    yc0 = 0000000;

var xa,xb,xc, ya,yb,yc;

var sh0,sh1,sh2,sh3,sh4,sh5;

var canvas,ctx,imageData,multiplier,pointlist;

var switch_canvas,switch_ctx;

//

function init_vars() {
  
  sh0 = 1 + (tword & 0300000) >> 15;
  sh1 = 1 + (tword & 0030000) >> 12;
  sh2 = 1 + (tword & 0003000) >> 9;
  sh3 = 1 + (tword & 0000300) >> 6;
  sh4 = 1 + (tword & 0000030) >> 3;
  sh5 = 1 + (tword & 0000003) ;
  
  xa = xa0; xb = xb0; xc = xc0;
  ya = ya0; yb = yb0; yc = yc0;

  canvas = document.getElementById('c');
  ctx = canvas.getContext('2d');
  imageData = ctx.createImageData(canvas.width, canvas.height);
  multiplier = canvas.width / 1024;
  pointlist = {};

  switch_canvas = document.getElementById('control');
  switch_ctx = switch_canvas.getContext('2d');
}

var ov = 0;  // overflow bit
function perform_add(num1,num2) {
  var ac = num1 + num2;
  ov = ac >> 18;
  ac = (ac + ov) & 0777777;
  if (ac == 0777777) ac = 0;
  return ac;
}

function perform_sub(num1,num2) {
  var diffsigns = ((num1 >> 17)^(num2 >> 17)) == 1;
  num1 += (num2 ^ 0777777);
  num1 = (num1 + (num1 >> 18)) & 0777777;
  if (num1 == 0777777) ac = 0;
  if (diffsigns && (num2 >> 17 == num1 >> 17)) ov=1;
  return num1;
}

function PLOT() {
  var px = (ac + 0400000) & 0777777;
  var py = 0777777 - (io + 0400000) & 0777777;
  
  px = (px >> 8) & 01777;
  py = (py >> 8) & 01777;
  console.log('px = ' + px + ', py = ' + py);
  queue_point(px,py);
}

function minsky() {
  ac = perform_add(xa,xb);
  ac >>= sh0;
  ya = perform_add(ya, ac);
  ac = perform_sub(ac, yb);
  ac >>= sh1;
  ac = (ac ^ 0777777) & 0777777;
  xa = perform_add(xa, ac);
  io = ya;
  PLOT();
  
  ac = perform_sub(xb,xc);
  ac >>= sh2;
  yb = perform_add(yb, ac);
  ac = perform_sub(ac, yc);
  ac >>= sh3;
  ac = (ac ^ 0777777) & 0777777;
  xb = perform_add(xb, ac);
  io = yb;
  PLOT();
  
  ac = perform_sub(xc,xa);
  ac >>= sh4;
  yc = perform_add(yc, ac);
  ac = perform_sub(ac, ya);
  ac >>= sh5;
  ac = (ac ^ 0777777) & 0777777;
  xc = perform_add(xc, ac);
  io = yc;
  PLOT();
}

// some canvas functions

function queue_point(px, py) {
  var key = px+','+py;
  pointlist[key] =  { x: px, y: py, alpha: 255 };
}


function draw_frame() {
  var k = Object.keys(pointlist);
  for (var i = 0; i < k.length; ++i) {
    if (pointlist[k[i]].alpha <= 0) {
      draw_point(pointlist[k[i]].x,pointlist[k[i]].y,0);
      delete(pointlist[k[i]]);
    } else {
      draw_point(pointlist[k[i]].x,pointlist[k[i]].y,pointlist[k[i]].alpha);
      pointlist[k[i]].alpha -= 16;
    }
  }
  ctx.putImageData(imageData, 0,0);
}


function draw_point(px, py, alpha) {
  var canvasx = Math.floor(px * multiplier), canvasy = Math.floor(py * multiplier);
  var offset = py * (canvas.width * 4) + (px * 4);
  console.log('canvasx = ' + canvasx + ', canvasy = ' + canvasy + ', alpha = ' + alpha);
  if (alpha > 0)  { // update pixel
    imageData.data[offset] = 255;
    imageData.data[offset + 1] = 255;
    imageData.data[offset + 2] = 0;
    imageData.data[offset + 3] = alpha;
  } else { // alpha = 0, which means remove this pixel
    imageData.data[offset] = imageData.data[offset+1] = imageData.data[offset+2] = imageData.data[offset+3] = 0;
  }
}

function draw_switches() {
  for (var i = 1; i <= 18; ++i) {
    switch_ctx.fillStyle = 'rgb(200,70,70)';
    switch_ctx.fillRect(i * 10 + Math.floor((i-1)/3) * 4, 15, 6, 15);
    switch_ctx.fillStyle = 'rgb(255,255,255)';
    switch_ctx.fillRect(i*10+1 +  + Math.floor((i-1)/3) * 4, 17, 3, 3);
  }
}
// main entry point
function runme() {
  init_vars();
  draw_switches();
}